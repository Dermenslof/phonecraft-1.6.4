package fr.ironcraft.mods.phonecraft.gui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.utils.ImageLoader;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

/**
 * @author Dermenslof
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneImages extends GuiPhoneIngame
{
	protected int prevImage;
	protected List<String> photos;
	protected BufferedImage buffered;
	protected boolean menuPhoto;
	protected ImageLoader imgLoader;
	protected static int image;
	protected int texture;
	protected Map<Integer, String> appsList = new HashMap<Integer, String>();
	protected static boolean rotate;
	
	public GuiPhoneImages (Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		this.imgLoader = new ImageLoader();
	}
	
    public void initGui()
    {
    	this.focus = true;
		this.ecran = 4;
		this.menuPhoto = true;
		this.photos = getImagesAvaiable();
		this.prevImage = -1;
    	super.initGui();
    }
	
    public void updateScreen()
    {
    	super.updateScreen();
    }
	
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    public void keyTyped(char par1, int par2)
    {
        super.keyTyped(par1, par2);
    }
    
    public void mouseClicked(int i, int j, int k)
	{
		super.mouseClicked(i, j, k);
		if (!this.mc.inGameHasFocus)
		{
			switch(this.bouton)
			{
			case 0:
				this.app = -1;
				this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc));
				this.ecran = 0;
				break;
			case 1:
				if(this.image != -1)
					this.mc.displayGuiScreen(new GuiPhoneEditImg(this.mc, new File(this.photos.get(this.image))));
				break;
			case 2:
				this.ecran = 2;
				break;
			case 3:
				this.rotate = !this.rotate;
				break;
			case 4:
				try
				{
					String name = this.photos.get(this.image);
					File f = new File(this.mc.mcDataDir, PhoneCraft.phoneFolder + "pictures/dcim/" + name);
					f.delete();
					this.photos = getImagesAvaiable();

					if(this.image >this.photos.size()-1)
						this.image--;
				}
				catch(Exception e)
				{
					this.image = 0;	
				}
				break;
			}
			//ecran images
			if(i >= this.width-106 && i <= this.width-14)
			{
				if(j >= this.height-184 && j <= this.height-29)
				{
					if(k == 1)
						this.menuPhoto = !this.menuPhoto;
				}
			}
		}
	}
    
    public void handleMouseInput()
    {
        int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
        super.handleMouseInput();
        if(Mouse.getEventButtonState())
        this.mouseClicked(x, y, Mouse.getEventButton());
    }
    
    
    public void drawScreen(int par1, int par2, float par3)
    {
    	super.drawScreen(par1, par2, par3);
    	drawImages(par1, par2, par3);
    	onMouseOverPhone(par1, par2);
    }
    
	public void drawImages(int par1, int par2, float par3)
	{
		this.drawRect(this.width-106+this.decalage, this.height-183, this.width-14+this.decalage, this.height-29, 0xff000000, 1);
		boolean doPop = false;
		boolean noImg = false;
		GL11.glPushMatrix();
			try
			{
				GL11.glEnable(GL11.GL_BLEND);
				String name = this.photos.get(this.image);
				File f = new File(this.mc.mcDataDir, PhoneCraft.phoneFolder + "pictures/dcim/" + name);
				if(f.exists())
				{
					int sizeX = 256;
					int sizeY = 256;
					if(this.prevImage != this.image)
					{
						this.prevImage = this.image;
						Image image = ImageIO.read(f);
						this.buffered = (BufferedImage) image;
						this.texture = this.imgLoader.setupTexture(this.buffered);
					}
					GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texture);
					if(this.rotate)
					{
						GL11.glTranslatef(this.width-106+91.75F+this.decalage, this.height-28-155, 0);
						GL11.glRotatef(+90F, 0, 0, 1);
						GL11.glScalef(0.602F, 0.35500256F, 1);
					}
					else
					{
						GL11.glTranslatef(this.width-106+this.decalage, this.height-20-120, 0);
						GL11.glScalef(0.360F, 0.21230769F, 1);
					}
					GL11.glColor4f(1,  1,  1,  this.transparency);
					this.drawTexturedModalRect(0, 0, 0, 0, sizeX, sizeY);
				}
				else
					noImg = true;
				doPop = true;
			}
			catch(Exception ex)
			{
				doPop = noImg = true;
			}
			if(noImg)
			{
				this.font.drawString(this, "pas d'image", this.width-60 -(this.font.getStringWidth("pas d'image")/2), this.height/2-40, 0xffd2d2d2, this.transparency);
				this.image = -1;
			}
			if(doPop)
				GL11.glPopMatrix();
			if(this.menuPhoto)
			{
				//barre head
				if(this.photos.size() != 0)
				{
					GL11.glPushMatrix();
						drawRect(this.width-106+this.decalage, this.height-28-155, this.width-106+this.decalage+92, this.height-28-155+7, 0xff222222, this.transparency);
						GL11.glTranslatef(this.width-56-0.5F,  this.height-28-157,  0);
						GL11.glScalef(0.5F, 0.5F, 1);
						this.font.drawString(this, String.valueOf(this.image+1)+"/"+String.valueOf(this.photos.size()), (int) (0-(this.font.getStringWidth(String.valueOf(this.image+1)+"/"+String.valueOf(this.photos.size()))/2F)/0.5F), 0, 0xffd2d2d2, this.transparency);
						GL11.glPopMatrix();
				}
				//fond boutons
				GL11.glPushMatrix();
					this.drawGradientRect(this.width-106+this.decalage, this.height-48, this.width-14+this.decalage, this.height-28, 0xff323232, 0xff111111, this.transparency);
					this.drawGradientRect(this.width-105+this.decalage, this.height-47, this.width-84+this.decalage, this.height-29, 0xff626262, 0xff424242, this.transparency);
					this.drawGradientRect(this.width-82+this.decalage, this.height-47, this.width-61+this.decalage, this.height-29, 0xff626262, 0xff424242, this.transparency);
					this.drawGradientRect(this.width-59+this.decalage, this.height-47, this.width-38+this.decalage, this.height-29, 0xff626262, 0xff424242, this.transparency);
					this.drawGradientRect(this.width-36+this.decalage, this.height-47, this.width-15+this.decalage, this.height-29, 0xff626262, 0xff424242, this.transparency);
					//icons boutons
					this.mc.renderEngine.bindTexture(texturePhone);
					GL11.glEnable(GL11.GL_BLEND);
					GL11.glColor4f(1,  1,  1, this.transparency);
					GL11.glTranslatef(this.width-103.5F+this.decalage, this.height-43, 0);
					for(int t=0; t<4; t++)
						this.drawTexturedModalRect(0+(t*23), 0, 110+(t+2)*14, 0, 15, 10);
				GL11.glPopMatrix();
			}
			this.drawRect(this.width-106+this.decalage, this.height-183, this.width-14+this.decalage, this.height-29, 0xff000000, 1f-this.transparency);
	}
	
	public List<String> getImagesAvaiable()
	{
		List<String> list = new ArrayList<String>();
		File dir = new File(this.mc.mcDataDir, PhoneCraft.phoneFolder + "pictures/dcim/");
		if (!dir.exists())
			dir.mkdirs();
		for(File f : dir.listFiles())
		{
			if(f.getPath().contains(".png"))
				list.add(f.getName());
		}
		return list;
	}
    
    private void onMouseOverPhone(int x, int y)
    {
		if(this.tactile && !this.mouseIsDrag && this.clickX >= 0)
		{
			if(-this.clickX+this.releaseX >40)
				this.image--;
			else if(-this.clickX+this.releaseX <-40)
				this.image++;
			this.tactile = false;
			this.clickX = this.releaseX = this.releaseY = this.clickY = -1;
		}
		if(this.image < 0)
			this.image = this.photos.size()-1;
		if(this.image > this.photos.size()-1)
			this.image = 0;
    	if(this.focus)
    	{
    		this.bouton = -1;
    		if(!this.animPhoto)
    		{
    			if(x >= this.width-106 && x <= this.width-14)
    			{
    				if(y >= this.height-191 && y <= this.height-29)
    				{
    					if(this.menuPhoto)
    					{
    						for(int t=0; t<4; t++)
    						{
    							if(x >= this.width-106+(t*23) && x <= this.width-84+(t*23) && y >= this.height-48 && y <= this.height-31)
    							{
    								GL11.glPushMatrix();
    									this.drawGradientRect(this.width-105+this.decalage+(t*23), this.height-47, this.width-84+this.decalage+(t*23), this.height-29, 0xff626262, 0x55000000, this.transparency-1.5F);
    								GL11.glPopMatrix();
    								this.bouton = t+1;
    							}
    						}
    					}
    				}
    			}
    			if(x >= this.width-71 && x <= this.width-51)
    			{
    				if(y >= this.height-19 && y <= this.height-13)
    				{
    					GL11.glPushMatrix();
    						GL11.glTranslatef(0.5F, 1.22F, 0);
    						GL11.glEnable(GL11.GL_BLEND);
    						GL11.glColor4f(1F,  1F,  1F,  0.3F);
    						this.mc.renderEngine.bindTexture(texturePhone);
    						this.drawTexturedModalRect(this.width-72+this.decalage, this.height-19, 0, 414/2+6, 50, 6);
    						GL11.glDisable(GL11.GL_BLEND);
    					GL11.glPopMatrix();
    					this.bouton = 0;
    				}
    			}
    		}
    	}
    }
}