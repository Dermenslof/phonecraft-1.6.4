package fr.ironcraft.mods.phonecraft.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.mods.PhoneCraft;

/**
 * @author Dermenslof
 */
@SideOnly(Side.CLIENT)
public class GuiPhone extends GuiPhoneIngame
{
	private boolean slide;
	
	public GuiPhone(Minecraft par1Minecraft, boolean slide)
	{
		super(par1Minecraft);
		this.slide = slide;
		if(this.slide)
			this.decalage = 214;
		else
			this.decalage = 0;
	}

	public void initGui()
	{	
		super.initGui();
	}

	public void updateScreen()
	{
		
	}

	public void keyTyped(char par1, int par2)
	{
		
	}
	
	public void mouseClicked(int i, int j, int k)
	{
		
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		this.drawRect(this.width-106+this.decalage, this.height-183, this.width-14+this.decalage, this.height-29, 0xff000000);
		drawAnimation();
	}
	
    private void drawAnimation()
    {
    	if(this.slide)
    	{
    		if(this.decalage > 0)
    			this.decalage = this.decalage-5 > 0 ? this.decalage-10 : 0;
    		if(this.decalage <= 0)
    			this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc));
    	}
    	else
    	{
    		if(this.decalage < 214)
    			this.decalage = this.decalage+5 < 210 ? this.decalage+10 : 214;
    		if(this.decalage >= 214){
                this.mc.displayGuiScreen((GuiScreen)null);
                this.mc.setIngameFocus();
    		}
    	}
    }
}


