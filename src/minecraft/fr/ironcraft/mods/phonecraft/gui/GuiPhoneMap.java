package fr.ironcraft.mods.phonecraft.gui;

import java.io.File;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import fr.ironcraft.mods.phonecraft.utils.MapRenderer;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemMap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * @author Dermenslof
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneMap extends GuiPhoneIngame
{
	private  MapRenderer mapRenderer;
	public  ItemMap map;
	public  ItemStack mapStack;
//	private ReiMinimap minimap;
	public static float slideX;
	public static float slideY;

	public GuiPhoneMap(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
		//		if(this.mapRenderer == null){
		//			this.mapRenderer = new MapRenderer(this.fontRenderer, this.mc.gameSettings, this.mc.renderEngine);
		//			this.mapStack = new ItemStack(Item.map);
		//			this.map = (ItemMap) this.mapStack.getItem();
		//			this.map.onCreated(this.mapStack, this.mc.theWorld, this.mc.thePlayer);
		//		}
		//this.minimap = new ReiMinimap();
	}

	public void initGui()
	{
		super.initGui();
	}

	public boolean doesGuiPauseGame()
	{
		return false;
	}

	public void mouseClicked(int i, int j, int k)
	{
		if (!this.mc.inGameHasFocus)
		{
			switch(this.bouton)
			{
			case 0:
				this.app = 0;
				this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc));
				break;
			case 1:
				//System.out.println(this.bouton);
				break;
			}
		}
		super.mouseClicked(i, j, k);
	}

	public void handleMouseInput()
	{
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
		/* gestion slide tactile */
		if (Mouse.isButtonDown(0))
		{
			if(!this.mouseIsDrag)
			{
				this.clickX = x;
				this.clickY = y;
				this.mouseIsDrag = true;
			}
			else
			{
				if(this.clickX != x || this.clickY != y)
					this.tactile= true;
				this.releaseX = x;
				this.releaseY = y;
			}
		}
		else
		{
			this.mouseIsDrag = false;
			this.slideX = 0;
			this.slideY = 0;
		}
		if(Mouse.getEventButtonState())
			this.mouseClicked(x, y, Mouse.getEventButton());
	}

	public void keyTyped(char par1, int par2)
	{
		super.keyTyped(par1, par2);
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);
		if(this.width > 427)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0, this.height-240, 0);
		}
//		ReiMinimap.instance.onTickInGame(this.mc);
		if(this.width > 427)
			GL11.glPopMatrix();
		this.onMouseOverPhone(par1,  par2);
	}

	public void updateScreen()
	{
		super.updateScreen();
	}

	private void onMouseOverPhone(int x, int y){

		if(this.tactile && this.mouseIsDrag && this.clickX >= 0)
		{
			this.slideX = -(-this.clickX+this.releaseX);
			this.slideY = -(-this.clickY+this.releaseY);
		}
		if(this.focus)
		{
			this.bouton = -1;
			if(x >= this.width-71 && x <= this.width-51)
			{
				if(y >= this.height-19 && y <= this.height-13)
				{
					GL11.glPushMatrix();
						GL11.glTranslatef(0.5F, 1.22F, 0);
						GL11.glEnable(GL11.GL_BLEND);
						GL11.glColor4f(1F,  1F,  1F,  0.3F);
						this.mc.renderEngine.bindTexture(new ResourceLocation("textures/gui/phone1.png"));
						this.drawTexturedModalRect(this.width-72+this.decalage, this.height-19, 0, 414/2+6, 50, 6);
						GL11.glDisable(GL11.GL_BLEND);
					GL11.glPopMatrix();
					this.bouton = 0;
				}
			}
		}
	}
}
