package fr.ironcraft.mods.phonecraft.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * @author Dermenslof
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneContacts extends GuiPhoneIngame
{
	public GuiPhoneContacts (Minecraft par1Minecraft)
	{
		super(par1Minecraft);
	}
	
    public void initGui()
    {
    	super.initGui();
    }
	
    public void updateScreen()
    {
    	super.updateScreen();
    }
	
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    public void keyTyped(char par1, int par2)
    {
        super.keyTyped(par1, par2);
    }
    
    public void mouseClicked(int i, int j, int k)
	{
    	if (!this.mc.inGameHasFocus)
    	{
    		switch(this.bouton)
    		{
    		case 0:
    			this.decalage = 0;
    			this.mc.displayGuiScreen(new GuiPhoneMenu(this.mc));
    			break;
    		}
    	}
		super.mouseClicked(i, j, k);
	}
    
    public void handleMouseInput()
    {
        int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
        super.handleMouseInput();
        if(Mouse.getEventButtonState())
        this.mouseClicked(x, y, Mouse.getEventButton());
    }
    
    
    public void drawScreen(int par1, int par2, float par3)
    {
    	super.drawScreen(par1, par2, par3);
    	this.font.drawString(this, "Contacts", this.width-61-(this.font.getStringWidth("Contacts")/2)+this.decalage, this.height-188,  0xffd2d2d2);
    	onMouseOverPhone(par1, par2);
    }
    
    private void onMouseOverPhone(int x, int y)
    {
    	if(this.focus)
    	{
    		this.bouton = -1;
    		if(x >= this.width-71 && x <= this.width-51)
    		{
    			if(y >= this.height-19 && y <= this.height-13)
    			{
    				GL11.glPushMatrix();
    					GL11.glTranslatef(0.5F, 1.22F, 0);
    					GL11.glEnable(GL11.GL_BLEND);
    					GL11.glColor4f(1F,  1F,  1F,  0.3F);
    					this.mc.renderEngine.bindTexture(GuiPhoneIngame.texturePhone);
    					this.drawTexturedModalRect(this.width-72+this.decalage, this.height-19, 0, 414/2+6, 50, 6);
    					GL11.glDisable(GL11.GL_BLEND);
    				GL11.glPopMatrix();
    				this.bouton = 0;
    			}
    		}
    	}
    }
}
