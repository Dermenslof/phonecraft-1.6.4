package fr.ironcraft.mods.phonecraft.gui;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet14BlockDig;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovementInput;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.utils.CustomFont;

/**
 * @author Dermenslof
 */
@SideOnly(Side.CLIENT)
public class GuiPhoneIngame extends GuiScreen
{
	protected static Minecraft mc;
	protected MovementInput move;

	protected float transparency;
	protected boolean exit;

	protected int mouseX;
	protected int mouseY;

	protected boolean focus;
	protected int decalage;

	protected static int ecran;
	protected int bouton;
	protected static int page;
	protected boolean mouseIsDrag;
	protected float clickX;
	protected float clickY;
	protected float releaseX;
	protected float releaseY;
	protected boolean tactile;
	protected CustomFont font;
	protected static boolean home;

	protected boolean animPhoto;
	protected float angle;
	protected int changePoint;
	protected float scale = 1;

	protected int app;

	protected GuiPhoneMenu Menu;

	public boolean hideGui;

	public static boolean isCamera;
	protected boolean shootCamera;

	private int rightClickDelayTimer  = 4 ;
	private int leftClickCounter = 10;

	private boolean isHittingBlock;
	private int currentBlockX;
	private int currentBlockY;
	private int currentblockZ;
	private ItemStack field_85183_f;
	private float curBlockDamageMP;
	private float stepSoundTickCounter;
	public float zoom = 1.0F;

	public static ResourceLocation texturePhone = new ResourceLocation("textures/gui/phone1.png");

	public GuiPhoneIngame (Minecraft par1Minecraft)
	{
		this.mc = par1Minecraft;
	}

	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);
		try
		{
			this.font = new CustomFont(this.mc, "TimesNewRoman", 10);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.focus = true;
	}

	public void updateScreen()
	{
		if (this.leftClickCounter > 0)
			--this.leftClickCounter;
		if (this.rightClickDelayTimer > 0)
			--this.rightClickDelayTimer;
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
		{
			if(!this.focus)
			{
				this.mc.inGameHasFocus = false;
				this.mc.mouseHelper.ungrabMouseCursor();
				this.focus = true;
			}
		}
		else
		{
			this.mc.inGameHasFocus = true;
			this.mc.mouseHelper.grabMouseCursor();
			this.focus = false;
		}
	}

	public boolean doesGuiPauseGame()
	{
		return false;
	}

	public void keyTyped(char par1, int par2)
	{
		if ((par2 == 1 || par1 == 'f'))
			this.exit = true;
	}

	public void handleMouseInput()
	{
		if (!Mouse.isButtonDown(1) || !this.mc.inGameHasFocus)
            this.mc.thePlayer.clearItemInUse();
		int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
		int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
		//gestion slide tactile
		if (Mouse.isButtonDown(0) && this.focus)
		{
			if(x >= this.width-106 && x <= this.width-14)
			{
				if(y >= this.height-184 && y <= this.height-49)
				{
					if(!this.mouseIsDrag)
					{
						this.clickX = x;
						this.clickY = y;
						this.mouseIsDrag = true;
					}
					else
					{
						if(this.clickX != x || this.clickY != y)
							this.tactile= true;
						this.releaseX = x;
						this.releaseY = y;
					}
				}
				else
					this.mouseIsDrag = false;
			}
			else
				this.mouseIsDrag = false;
		}
		else
			this.mouseIsDrag = false;
		int w = Mouse.getDWheel();
		if (w != 0 && !this.focus)
		{
			if (w > 0)
				this.mc.thePlayer.inventory.changeCurrentItem(1);
			else if (w < 0)
				this.mc.thePlayer.inventory.changeCurrentItem(-1);
		}
	}

	public void mouseClicked(int i, int j, int k)
	{
		if(Mouse.isButtonDown(1) && k == 0 && this.mc.inGameHasFocus)
			return;
		if (this.mc.inGameHasFocus && !(this instanceof GuiPhoneCamera))
			this.clickMouse(k);
	}
	
	private void setMovement()
	{
		boolean up = Keyboard.isKeyDown(this.mc.gameSettings.keyBindForward.keyCode);
		boolean down = Keyboard.isKeyDown(this.mc.gameSettings.keyBindBack.keyCode);
		boolean jump = Keyboard.isKeyDown(this.mc.gameSettings.keyBindJump.keyCode);
		boolean left = Keyboard.isKeyDown(this.mc.gameSettings.keyBindLeft.keyCode);
		boolean right = Keyboard.isKeyDown(this.mc.gameSettings.keyBindRight.keyCode);
		if(jump && this.mc.thePlayer.onGround)
			this.mc.thePlayer.motionY = 0.4F;
		float dir = 180;
		float power = 0;

		if (left == right)
		{
			dir = 180;

			if(up && !down)
				power = -1;
			else if(down && !up)
				power = 1;
		}
		else
		{
			if(up == down)
			{
				dir = -90;
				if(left && !right)
					power = 1;
				else if(right && !left)
					power = -1;
			}
			else
			{
				if(up && !down && left && !right)
				{
					dir = -45;
					power = 1;
				}
				else if(up && !down && !left && right)
				{
					dir = 45;
					power = 1;
				}
				if(!up && down && left && !right)
				{
					dir = 45;
					power = -1;
				}
				else if(!up && down && !left && right)
				{
					dir = -45;
					power = -1;
				}
			}
		}
		this.mc.thePlayer.motionZ = (double)(MathHelper.cos((this.mc.thePlayer.rotationYaw + dir) / 180.0F * (float)Math.PI) * power * 0.25F);
		this.mc.thePlayer.motionX = (double)(-MathHelper.sin((this.mc.thePlayer.rotationYaw + dir) / 180.0F * (float)Math.PI) * power * 0.25F);
	}

	private void drawBackground()
	{
		setMovement();
		GL11.glPushMatrix();
			if(this.ecran == 1)
			{
				GL11.glPopMatrix();
				return;
			}
			//image telephone
			GL11.glColor4f(1,  1,  1,  3-this.scale);
			GL11.glTranslatef(this.width/2, this.height/2, 0);
			GL11.glRotatef(this.angle, 0, 0, 1);
			GL11.glScalef(this.scale, this.scale, 1);
			GL11.glTranslatef(-this.width/2-this.changePoint/1.38F, -this.height/2-this.changePoint/15, 0);
			GL11.glPushMatrix();
				GL11.glTranslatef(-0.5F, 0F, 0);
				this.mc.renderEngine.bindTexture(texturePhone);
				this.drawTexturedModalRect(this.width-110+this.decalage, this.height-210, 3, 0, 110, 414/2);
			GL11.glPopMatrix();
			Date d = new Date();
			String h = d.getHours()<10 ? "0"+String.valueOf(d.getHours()) : String.valueOf(d.getHours());
			String m =  d.getMinutes()<10 ? "0"+String.valueOf(d.getMinutes()) : String.valueOf(d.getMinutes());
			//time (real)
			GL11.glPushMatrix();
				GL11.glScalef(0.5F, 0.5F, 1);
				GL11.glTranslatef((this.width-28.5F+this.decalage)/0.5F, (this.height-192.5F)/0.5F, 0);
				this.font.drawString(this, h, 0, 0, 0xffd2d2d2);
				this.font.drawString(this, ":", 12, -1, 0xffd2d2d2);
				this.font.drawString(this, m, 16, 0, 0xffd2d2d2);
			GL11.glPopMatrix();
			//reseau
			GL11.glPushMatrix();
				GL11.glTranslatef(this.width-105F+this.decalage, this.height-192, 0);
				GL11.glScalef(0.5F, 0.5F, 1);
				this.font.drawString(this, "ICtelecom", 0, 0, 0xffd2d2d2);
			GL11.glPopMatrix();
			this.drawRect(this.width-106+this.decalage, this.height-183, this.width-14+this.decalage, this.height-29, 0xff000000, this.ecran == 4 ? 0 : this.ecran == -1 ? this.transparency : 1f-this.transparency);
			GL11.glColor4f(1,  1,  1,  1);
		GL11.glPopMatrix();
	}

	public void drawScreen(int par1, int par2, float par3)
	{
		if(this.exit)
		{
			this.transparency -= 0.1F;
			if(this.transparency <= -0.02F)
				this.mc.displayGuiScreen(new GuiPhone(this.mc, false));
		}
		else
		{
			this.transparency +=0.1F;
			if(this.transparency >= 2.0F)
				this.transparency = 2.0F;
		}
		this.mouseX = par1;
		this.mouseY = par2;
		drawBackground();
	}

	protected void drawGradientRect(int par1, int par2, int par3, int par4, int par5, int par6, float trans)
	{
		float var7 = (float)(par5 >> 24 & 255) / 255.0F;
		float var8 = (float)(par5 >> 16 & 255) / 255.0F;
		float var9 = (float)(par5 >> 8 & 255) / 255.0F;
		float var10 = (float)(par5 & 255) / 255.0F;
		float var11 = (float)(par6 >> 24 & 255) / 255.0F;
		float var12 = (float)(par6 >> 16 & 255) / 255.0F;
		float var13 = (float)(par6 >> 8 & 255) / 255.0F;
		float var14 = (float)(par6 & 255) / 255.0F;
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		Tessellator var15 = Tessellator.instance;
		var15.startDrawingQuads();
		var15.setColorRGBA_F(var8, var9, var10, trans);
		var15.addVertex((double)par3, (double)par2, (double)this.zLevel);
		var15.addVertex((double)par1, (double)par2, (double)this.zLevel);
		var15.setColorRGBA_F(var12, var13, var14, trans);
		var15.addVertex((double)par1, (double)par4, (double)this.zLevel);
		var15.addVertex((double)par3, (double)par4, (double)this.zLevel);
		var15.draw();
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	protected void drawRoundedGradientRect(int x, int y, int x1, int y1, int color, int color2, int radius, float trans)
	{
		int newX = Math.abs(x+radius);
		int newY = Math.abs(y+radius);
		int newX1 = Math.abs(x1-radius);
		int newY1 = Math.abs(y1-radius);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		drawRect(newX, newY, newX1, newY1, color, trans);
		drawRect(x, newY, newX, newY1, color, trans);
		drawRect(newX1, newY, x1, newY1, color, trans);
		drawRect(newX, y, newX1, newY, color2, trans);
		drawRect(newX, newY1, newX1, y1, color2, trans);
		drawQuarterCircle(newX+1,newY,radius,0,color, trans);
		drawQuarterCircle(newX1,newY,radius,1,color, trans);
		drawQuarterCircle(newX+1,newY1,radius,2,color2, trans);
		drawQuarterCircle(newX1,newY1,radius,3,color2, trans);
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	protected void drawRect(int par0, int par1, int par2, int par3, int par4, float trans)
	{
		int var5;
		if (par0 < par2)
		{
			var5 = par0;
			par0 = par2;
			par2 = var5;
		}
		if (par1 < par3)
		{
			var5 = par1;
			par1 = par3;
			par3 = var5;
		}
		float var6 = (float)(par4 >> 16 & 255) / 255.0F;
		float var7 = (float)(par4 >> 8 & 255) / 255.0F;
		float var8 = (float)(par4 & 255) / 255.0F;
		Tessellator var9 = Tessellator.instance;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glColor4f(var6, var7, var8, trans);
		var9.startDrawingQuads();
		var9.addVertex((double)par0, (double)par3, 0.0D);
		var9.addVertex((double)par2, (double)par3, 0.0D);
		var9.addVertex((double)par2, (double)par1, 0.0D);
		var9.addVertex((double)par0, (double)par1, 0.0D);
		var9.draw();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
	}

	public void drawRoundedRect(int x, int y, int x1, int y1, int radius, int color, float trans)
	{
		int newX = Math.abs(x+radius);
		int newY = Math.abs(y+radius);
		int newX1 = Math.abs(x1-radius);
		int newY1 = Math.abs(y1-radius);
		drawRect(newX, newY, newX1, newY1, color, trans);
		drawRect(x, newY, newX, newY1, color, trans);
		drawRect(newX1, newY, x1, newY1, color, trans);
		drawRect(newX, y, newX1, newY, color, trans);
		drawRect(newX, newY1, newX1, y1, color, trans);
		drawQuarterCircle(newX,newY,radius,0,color, trans);
		drawQuarterCircle(newX1,newY,radius,1,color, trans);
		drawQuarterCircle(newX,newY1,radius,2,color, trans);
		drawQuarterCircle(newX1,newY1,radius,3,color, trans);
	}

	public void drawQuarterCircle(int x, int y, int radius, int mode, int color, float trans)
	{
		float var6 = (float)(color >> 16 & 255) / 255.0F;
		float var7 = (float)(color >> 8 & 255) / 255.0F;
		float var8 = (float)(color & 255) / 255.0F;
		disableDefaults();
		GL11.glColor4d(var6, var7, var8, trans);
		GL11.glBegin(GL11.GL_POLYGON);
		GL11.glVertex2d(x, y);
		int start = 0;
		int xradius = radius;
		int yradius = radius;
		switch (mode)
		{
		case 0:
			xradius = yradius = -radius;
			break;
		case 1:
			start = 90;
			break;
		case 2:
			start = 90;
			xradius = yradius = -radius;
			break;
		case 3:
		}
		for (int i = start; i <= start + 90; i++)
		{
			double theta_radian = i*Math.PI / 180D;
			GL11.glVertex2d(x + Math.sin(theta_radian) * xradius, y + Math.cos(theta_radian) * yradius);
		}
		GL11.glEnd();
		enableDefaults();
	}

	public void setupOverlayRendering()
	{
		GL11.glClear(256);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0.0D, this.width, this.height, 0.0D, 1000D, 3000D);
		GL11.glMatrixMode(5888 /*GL_MODELVIEW0_ARB*/);
		GL11.glLoadIdentity();
		GL11.glTranslatef(0.0F, 0.0F, -2000F);
	}
	
	public void disableDefaults()
	{
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}
	
	public void enableDefaults()
	{
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	private void clickMouse(int par1)
	{
		if ((par1 != 0 || this.leftClickCounter <= 0) || (par1 == 1 && this.rightClickDelayTimer <= 0))
		{
			if (par1 == 0)
				this.mc.thePlayer.swingItem();
			if (par1 == 1)
				this.rightClickDelayTimer = 4;
			this.leftClickCounter = 4;
			boolean flag = true;
			ItemStack itemstack = this.mc.thePlayer.inventory.getCurrentItem();
			if (this.mc.objectMouseOver == null)
			{
				if (par1 == 0 && this.mc.playerController.isNotCreative())
					this.leftClickCounter = 10;
			}
			else if (this.mc.objectMouseOver.typeOfHit == EnumMovingObjectType.ENTITY)
			{
				if (par1 == 0)
					this.mc.playerController.attackEntity(this.mc.thePlayer, this.mc.objectMouseOver.entityHit);
				if (par1 == 1 && this.mc.playerController.func_78768_b(this.mc.thePlayer, this.mc.objectMouseOver.entityHit))
					flag = false;
			}
			else if (this.mc.objectMouseOver.typeOfHit == EnumMovingObjectType.TILE)
			{
				int j = this.mc.objectMouseOver.blockX;
				int k = this.mc.objectMouseOver.blockY;
				int l = this.mc.objectMouseOver.blockZ;
				int i1 = this.mc.objectMouseOver.sideHit;
				if (par1 == 0)
				{
					this.mc.playerController.clickBlock(j, k, l, this.mc.objectMouseOver.sideHit);
					if (this.isHittingBlock)
						this.mc.thePlayer.sendQueue.addToSendQueue(new Packet14BlockDig(1, this.currentBlockX, this.currentBlockY, this.currentblockZ, i1));
					this.mc.thePlayer.sendQueue.addToSendQueue(new Packet14BlockDig(0, j, k, l, this.mc.objectMouseOver.sideHit));
					int i = this.mc.theWorld.getBlockId(j, k, l);
					if (i > 0 && this.curBlockDamageMP == 0.0F)
						Block.blocksList[i1].onBlockClicked(this.mc.theWorld, j, k, l, this.mc.thePlayer);
					if (i > 0 && Block.blocksList[i1].getPlayerRelativeBlockHardness(this.mc.thePlayer, this.mc.thePlayer.worldObj, j, k, l) >= 1.0F)
						this.mc.playerController.onPlayerDestroyBlock(j, k, l, i1);
					else
					{
						this.isHittingBlock = true;
						this.currentBlockX = j;
						this.currentBlockY = k;
						this.currentblockZ = l;
						this.field_85183_f = this.mc.thePlayer.getHeldItem();
						this.curBlockDamageMP = 0.0F;
						this.stepSoundTickCounter = 0.0F;
						this.mc.theWorld.destroyBlockInWorldPartially(this.mc.thePlayer.entityId, this.currentBlockX, this.currentBlockY, this.currentblockZ, (int)(this.curBlockDamageMP * 10.0F) - 1);
					}
				}
				else
				{
					int j1 = itemstack != null ? itemstack.stackSize : 0;
					boolean result = !ForgeEventFactory.onPlayerInteract(this.mc.thePlayer, Action.RIGHT_CLICK_BLOCK, j, k, l, i1).isCanceled();
					if (result && this.mc.playerController.onPlayerRightClick(this.mc.thePlayer, this.mc.theWorld, itemstack, j, k, l, i1, this.mc.objectMouseOver.hitVec))
					{
						flag = false;
						this.mc.thePlayer.swingItem();
					}
					if (itemstack == null)
						return;
					if (itemstack.stackSize == 0)
						this.mc.thePlayer.inventory.mainInventory[this.mc.thePlayer.inventory.currentItem] = null;
					else if (itemstack.stackSize != j1 || this.mc.playerController.isInCreativeMode())
						this.mc.entityRenderer.itemRenderer.resetEquippedProgress();
				}
			}
			if (flag && par1 == 1)
			{
				ItemStack itemstack1 = this.mc.thePlayer.inventory.getCurrentItem();
				boolean result = !ForgeEventFactory.onPlayerInteract(this.mc.thePlayer, Action.RIGHT_CLICK_AIR, 0, 0, 0, -1).isCanceled();
				if (result && itemstack1 != null && this.mc.playerController.sendUseItem(this.mc.thePlayer, this.mc.theWorld, itemstack1))
					this.mc.entityRenderer.itemRenderer.resetEquippedProgress2();
			}
		}
	}
	
	public CustomFont getFont()
	{
		return this.font;
	}
	
	public int getDecalage()
	{
		return this.decalage;
	}

}
