package fr.ironcraft.mods.phonecraft.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModClassLoader;
import net.minecraft.client.Minecraft;
import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.api.Application;

/**
* @author Thog92
*/
class JarFilter implements FilenameFilter
{
	public boolean accept(File dir, String name)
	{
		return (name.endsWith(".zip") || name.endsWith(".jar"));
	}
}

/**
* @author Thog92
*/
public class AppServiceLoader
{
	private List<Application> appCollection;

	//Class Loader 
	private ModClassLoader ucl;

	public AppServiceLoader()
	{
		appCollection = new ArrayList<Application>();
	}
	
	public void search(File dir) throws Exception
	{
		if (dir.isFile())
			return;
		else if(!dir.exists())
		{
			System.out.println("[PhoneCraft] create apps folder");
			dir.mkdirs();
		}
		File[] files = dir.listFiles(new JarFilter());
		for (File f : files)
			findClassesInJar(Application.class, f);
	}
	
	public synchronized boolean findClassesInJar(final Class<?> baseInterface, final File jarFullPath) throws Exception
	{
		
		final List<String> classesTobeReturned = new ArrayList<String>();
		if (!jarFullPath.isDirectory())
		{
			@SuppressWarnings("resource")
			//Already close at end by FileOutputStream and InputStream
			JarFile jar = new JarFile(jarFullPath);
			String jarName = jar.getName().replace(".zip", "").replace(PhoneCraft.phoneFolder + "apps/", "");
			String zipname = jarFullPath.getName().replace(".zip", "");
			File resourcesFile = new File(jarFullPath.getParentFile(), "resources/" + zipname);
			
			for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements();)
			{
				JarEntry entry = (JarEntry) enums.nextElement();
				String image = entry.getName().replace(zipname, "").replace("resources/", "");
				if (entry.getName().endsWith(".png") || entry.getName().endsWith(".jpg"))
				{
					File dest = new File(resourcesFile, image);
					System.out.println("[PhoneCraft] extract image " + image + " to " + dest.getPath());
					if (!resourcesFile.exists())
						resourcesFile.mkdir();
					InputStream is = jar.getInputStream(entry);
					//On verifie que le fichier existe avant d'y ajouter du contenu
					if(!dest.exists())
					{
						//on verifie que son parent existe
						if(!dest.getParentFile().exists())
						{
							dest.getParentFile().mkdirs();
						}
						dest.createNewFile();
					}
					FileOutputStream fos = new FileOutputStream(dest);
					while (is.available() > 0)
						fos.write(is.read());
					fos.close();
					is.close();
				}
			}
			String currentThreadName = Thread.currentThread().getName();
			// jarName is relative location of jar wrt.
			JarInputStream jarFile = null;
			ClassLoader ucl2 = Loader.instance().getModClassLoader();
			if(ucl2 instanceof ModClassLoader)
			{
				 ucl = (ModClassLoader) Loader.instance().getModClassLoader();
				 ucl.addFile(jarFullPath);
			}
			jarFile = new JarInputStream(new FileInputStream(jarFullPath));
			JarEntry jarEntry;
			while (true)
			{
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null)
					break;
				if (jarEntry.getName().endsWith(".class"))
				{
					String classname = jarEntry.getName().replaceAll("/", "\\.");
					classname = classname.substring(0, classname.length() - 6);
					if (!classname.contains("$"))
					{
						try
						{
							final Class<?> myLoadedClass = Class.forName(classname, true, ucl);
							if (baseInterface.isAssignableFrom(myLoadedClass))
							{
								Application app = (Application) myLoadedClass.newInstance();
								appCollection.add(app);
								System.out.println("[PhoneApps] " + app.appname() + " "+ app.version() + " ---> DONE");
								return true;
							}
							else
							{
								Class.forName(classname, true, ucl);
								ucl.loadClass(classname);
								myLoadedClass.newInstance();
							}
						}
						catch (final ClassNotFoundException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
			jarFile.close();
		}
		return false;
	}

	public void search(String directory) throws Exception
	{
		File dir = new File(directory);
		search(dir);
	}
	
	public List<Application> getAppCollection()
	{
		return appCollection;
	}

	public void setAppCollection(List<Application> appCollection)
	{
		this.appCollection = appCollection;
	}
}