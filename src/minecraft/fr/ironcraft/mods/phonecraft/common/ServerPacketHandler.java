package fr.ironcraft.mods.phonecraft.common;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

import fr.ironcraft.mods.phonecraft.tileentities.TileEntityQrCode;

/**
 * @author Dermenslof
 */
public class ServerPacketHandler implements IPacketHandler
{
	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload payload, Player player)
	{
		DataInputStream data = new DataInputStream(new ByteArrayInputStream(payload.data));
		if (payload.channel.equals("QrCode"))
			handlePacketQrCode(payload);
	}

	private void handlePacketQrCode(Packet250CustomPayload packet)
	{
		DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));
		int wName = 0;
		int x, y, z;
		String data;
		try
		{
			wName = inputStream.readInt();
			x = inputStream.readInt();
			y = inputStream.readInt();
			z = inputStream.readInt();
			data = inputStream.readUTF();
			World w = FMLCommonHandler.instance().getMinecraftServerInstance().worldServerForDimension(wName);
			TileEntity tile = w.getBlockTileEntity(x, y, z);
			if (tile != null)
			{
				if (tile instanceof TileEntityQrCode)
				{
					TileEntityQrCode tileQrCode = (TileEntityQrCode)tile;
					tileQrCode.textureID = -1;
					tileQrCode.texture = data;
					tileQrCode.img = null;
				}
			}
		}
		catch (IOException e){}
		PacketDispatcher.sendPacketToAllPlayers(packet);
	}
}
