package fr.ironcraft.mods.phonecraft.client;

import net.minecraft.client.Minecraft;
import fr.ironcraft.mods.phonecraft.api.Application;
import fr.ironcraft.mods.phonecraft.gui.GuiPhoneImages;
import fr.ironcraft.mods.phonecraft.gui.GuiPhoneIngame;

/**
 * @author Thog92
 */
class SystemApp implements Application
{
	private String appname;
	private String version;
	private GuiPhoneIngame gui;

	public SystemApp(String string, String s, GuiPhoneIngame g)
	{
		this.appname = string;
		this.version = s;
		this.gui = g;
	}

	@Override
	public String appname()
	{
		return appname;
	}

	@Override
	public String version()
	{
		return version;
	}

	@Override
	public GuiPhoneIngame ScreenInstance()
	{
		return gui;
	}
}
