package fr.ironcraft.mods.phonecraft.client;

import java.util.EnumSet;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

/**
 * @author Dermenslof
 */
public class ClientTickHandler implements ITickHandler
{

	public void tickStart(EnumSet<TickType> type, Object... tickData)
	{ 
		
	}

	public void tickEnd(EnumSet<TickType> type, Object... tickData)
	{ 
		
	} 

	@Override 
	public EnumSet<TickType> ticks() 
	{ 
		return EnumSet.of(TickType.CLIENT); 
	}

	@Override
	public String getLabel()
	{
		return null;
	}
}
