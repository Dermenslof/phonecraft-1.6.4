package fr.ironcraft.mods.phonecraft.client;

import cpw.mods.fml.client.FMLClientHandler;
import fr.ironcraft.mods.phonecraft.gui.GuiPhoneCamera;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.ForgeSubscribe;

public class RenderOverlayHandler
{
	@ForgeSubscribe
	public void onRender(RenderGameOverlayEvent event)
	{
		if (!(FMLClientHandler.instance().getClient().currentScreen instanceof GuiPhoneCamera))
			return;
		if (!event.type.equals(ElementType.ALL))
			event.setCanceled(true);
	}
}
