package fr.ironcraft.mods.phonecraft.client;

import net.minecraftforge.client.event.sound.PlayBackgroundMusicEvent;
import net.minecraftforge.client.event.sound.PlaySoundEffectEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.client.event.sound.PlayStreamingEvent;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class SoundHandler
{
    @ForgeSubscribe
    public void loadSound(SoundLoadEvent event)
    {
        try
        {
        	event.manager.soundPoolSounds.addSound("phonecraft:clic/clicPhoto1.wav");
        	event.manager.soundPoolSounds.addSound("phonecraft:clic/clicPhoto2.wav");
        	event.manager.soundPoolSounds.addSound("phonecraft:clic/clicPhoto3.wav");
        	event.manager.soundPoolSounds.addSound("phonecraft:clic/clicPhoto4.wav");
        	System.out.println("[PhoneCraft] all sound \"clicPhoto.wav\" loaded");
        }
        catch (Exception e)
        {
        	System.out.println("[PhoneCraft] one or more sound \"clic.wav\" can't loaded");
        	e.printStackTrace();
        }
    }
}
