package fr.ironcraft.mods.phonecraft.client;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import de.cuina.fireandfuel.CodecJLayerMP3;
import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.common.CommonProxy;
import fr.ironcraft.mods.phonecraft.renders.RenderTileEntityQrCode;
import fr.ironcraft.mods.phonecraft.tileentities.TileEntityQrCode;
import fr.ironcraft.mods.phonecraft.utils.ImageLoader;
//import minebox.StreamSoundThread;

/**
 * @author Dermenslof
 */
public class ClientProxy extends CommonProxy 
{
	public static int id = 4000;
	public static ImageLoader imageLoader;
	public static int renderQrCodeID;
	protected AppRegistry appRegistry;
	public Minecraft mc;
	public Contact contact;

	@Override
	public void registerRenderThings()
	{
		mc = Minecraft.getMinecraft();
		try
		{
			SoundSystemConfig.setCodec("mp3", CodecJLayerMP3.class);
		}
		catch (SoundSystemException e)
		{
			System.err.println("error linking with the LibraryJavaSound plug-in");
			e.printStackTrace();
		}
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		TickRegistry.registerTickHandler(new ClientTickHandler(), Side.CLIENT);
		imageLoader = new ImageLoader();
		renderQrCodeID = RenderingRegistry.getNextAvailableRenderId();
		KeyBindingRegistry.registerKeyBinding(new ClientKeyHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityQrCode.class, new RenderTileEntityQrCode());
		appRegistry = new AppRegistry(mc);
		contact = new Contact();
		contact.refreshList();
	}

	public static void resetId()
	{
		id = 4000;
	}
}
