package fr.ironcraft.mods.phonecraft.client;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.tileentities.TileEntityQrCode;

public class ClientPacketHandler implements IPacketHandler
{
	public void onPacketData(INetworkManager manager, Packet250CustomPayload payload, Player player)
	{
		DataInputStream data = new DataInputStream(new ByteArrayInputStream(payload.data));
		if (payload.channel.equals("QrCode"))
			handlePacketQrCode(payload);
	}

	private void handlePacketQrCode(Packet250CustomPayload packet)
	{
		DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));
		int wName = 0;
		int x, y, z;
		String data;
		try
		{
			wName = inputStream.readInt();
			x = inputStream.readInt();
			y = inputStream.readInt();
			z = inputStream.readInt();
			data = inputStream.readUTF();
			World w = FMLClientHandler.instance().getClient().theWorld;
			TileEntity tile = w.getBlockTileEntity(x, y, z);
			if (tile != null)
			{
				if (tile instanceof TileEntityQrCode)
				{
					TileEntityQrCode tileQrCode = (TileEntityQrCode)tile;
					tileQrCode.textureID = -1;
					tileQrCode.data = data;
					tileQrCode.texture = PhoneCraft.urlFiles + "/qrcodes/" + tileQrCode.xCoord + "_" + tileQrCode.yCoord + "_" + tileQrCode.zCoord + ".png";
					tileQrCode.img = null;
				}
			}
		}
		catch (IOException e){}
	}
}