package fr.ironcraft.mods.phonecraft.client;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.stats.AchievementList;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;
import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.gui.GuiPhone;

/**
 * @author Dermenslof
 */
public class ClientKeyHandler extends KeyHandler
{
	static KeyBinding phoneKey = new KeyBinding("Phone", Keyboard.KEY_F);

	public ClientKeyHandler()
	{

		super(new KeyBinding[]{phoneKey}, new boolean[]{false});
	}

	@Override
	public String getLabel()
	{
		return "mykeybindings";
	}

	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat)
	{
			
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd)
	{
		FMLClientHandler client = FMLClientHandler.instance();
		Minecraft mc = client.getClient();
		EntityPlayer p = mc.thePlayer;
		if (mc.currentScreen == null)
		{
			if (kb == phoneKey)
			{
				p.triggerAchievement(AchievementList.openInventory);
				p.triggerAchievement(PhoneCraft.phoneAchievement);
				client.displayGuiScreen(p, new GuiPhone(mc, true));
			}
		}
	}

	@Override
	public EnumSet<TickType> ticks()
	{
		return EnumSet.of(TickType.CLIENT);
	}

}
