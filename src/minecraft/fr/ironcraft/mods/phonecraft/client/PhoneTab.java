package fr.ironcraft.mods.phonecraft.client;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.mods.PhoneCraft;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class PhoneTab extends CreativeTabs
{
	public PhoneTab(int tabId, String name)
	{
		super(tabId, name);
	}
	
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex()
	{
		return PhoneCraft.qrCode.blockID;
	}
	
	public String getTranslatedTabLabel()
	{
		return "PhoneTab";
	}
}
