package fr.ironcraft.mods.phonecraft.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import fr.ironcraft.mods.PhoneCraft;

public class Contact
{
	private String ext = ".pcc";
	private List<String> contactList = new ArrayList<String>();
	private File dir = new File(Minecraft.getMinecraft().mcDataDir, PhoneCraft.phoneFolder + "contacts");

	public void refreshList()
	{
		contactList.clear();
		try
		{
			if(!dir.exists())
			{
				System.out.println("[PhoneCraft] create contacts folder");
				dir.mkdirs();
			}
			File[] files = dir.listFiles();
			for (File f : files)
			{
				if (f.getName().endsWith(ext))
				{
					if (isValid(f.getName().replace(ext, "")))
						contactList.add(f.getName().replace(ext, ""));
				}
			}
			System.out.println("[PhoneCraft] " + contactList.size() + " contacts found");
		}
		catch (Exception e)
		{
			System.out.println("[PhoneCraft] Contact not found");
		}
	}

	public boolean isValid(String name)
	{
		try
		{
			URL url = new URL("https://minecraft.net/haspaid.jsp?user=" + name);
			URLConnection connect = url.openConnection();
			BufferedReader buff = new BufferedReader(new InputStreamReader(connect.getInputStream()));
			if (buff.readLine().startsWith("true"))
				return true;
			else
			{
				System.out.println("[WARNING] " + name + " is not valid : Remove");
				//remove(name);
			}
		} 
		catch (IOException e)
		{
			System.out.println("[WARNING] connection lost");
		}
		return false;
	}

	public boolean addNew(String name)
	{
		if (isValid(name) && !contactList.contains(name))
		{
			contactList.add(name);
			try
			{
				new File(dir, name + ext).createNewFile();
			}
			catch (IOException e)
			{
				System.out.println("[PhoneCraft] New contact was not create");
			}
			return (true);
		}
		return (false);
	}

	public void remove(String name)
	{
		new File(dir, name + ext).delete();
		contactList.remove(name);
	}

}