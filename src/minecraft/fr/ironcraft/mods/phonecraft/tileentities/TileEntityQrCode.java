package fr.ironcraft.mods.phonecraft.tileentities;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import fr.ironcraft.mods.PhoneCraft;
import fr.ironcraft.mods.phonecraft.utils.StringUtils;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet130UpdateSign;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;

/**
 * @author Dermenslof
 */
public class TileEntityQrCode extends TileEntity
{
	public String data = "";
	public String texture =	"";
	public int textureID = -1;
	public BufferedImage img;
	
    public Packet getDescriptionPacket()
	{
    	int wName = this.worldObj.getWorldInfo().getVanillaDimension();;
		String data = this.texture;
		ByteArrayOutputStream bos = new ByteArrayOutputStream(16 + data.length());
        DataOutputStream outputStream = new DataOutputStream(bos);
        try
        {
        	outputStream.writeInt(wName);
        	outputStream.writeInt(this.xCoord);
        	outputStream.writeInt(this.yCoord);
        	outputStream.writeInt(this.zCoord);
        	outputStream.writeUTF(data);
        }
        catch (Exception ex)
        {
        	ex.printStackTrace();
        }
        Packet250CustomPayload packet = new Packet250CustomPayload();
        packet.channel = "QrCode";
        packet.data = bos.toByteArray();
        packet.length = bos.size();
        return packet;
	}
	
	@Override
    public void readFromNBT(NBTTagCompound par1NBTTagCompound)
    {
    	super.readFromNBT(par1NBTTagCompound);
    	this.texture = par1NBTTagCompound.getString("texture");
    }

    @Override
    public void writeToNBT(NBTTagCompound par1NBTTagCompound)
    {
       	super.writeToNBT(par1NBTTagCompound);
    	par1NBTTagCompound.setString("texture", this.texture);
    }
    
    @Override
    public Block getBlockType()
    {
    	return PhoneCraft.qrCode;
    }
}