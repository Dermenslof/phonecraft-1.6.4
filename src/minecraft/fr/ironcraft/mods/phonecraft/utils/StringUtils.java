package fr.ironcraft.mods.phonecraft.utils;

public class StringUtils
{
	public static String stringToHex(String str)
	{ 
		char[] chars = str.toCharArray();
		StringBuffer strBuffer = new StringBuffer();
		for (int i = 0; i < chars.length; i++)
			strBuffer.append("%" + Integer.toHexString((int) chars[i]));
		return strBuffer.toString();
	}
	
	public static String hexToString(String hex)
	{
		  StringBuilder sb = new StringBuilder();
		  StringBuilder temp = new StringBuilder();
		  for( int i=0; i<hex.length()-1; i+=2 )
		  {
		      String output = hex.substring(i, (i + 2));
		      int decimal = Integer.parseInt(output, 16);
		      sb.append((char)decimal);
		      temp.append(decimal);
		  }
		  return sb.toString();
	 }
}
