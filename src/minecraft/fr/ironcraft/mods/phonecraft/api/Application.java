package fr.ironcraft.mods.phonecraft.api;

import net.minecraft.client.Minecraft;
import fr.ironcraft.mods.phonecraft.gui.GuiPhoneIngame;

/**
* @author Thog92
*/
public interface Application
{
 public abstract String appname();
 public abstract String version();
 public abstract GuiPhoneIngame ScreenInstance();
}
