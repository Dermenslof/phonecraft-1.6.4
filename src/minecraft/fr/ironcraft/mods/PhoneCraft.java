package fr.ironcraft.mods;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.BlockGlowStone;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.Configuration;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkMod.SidedPacketHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

import fr.ironcraft.mods.phonecraft.blocks.BlockFlash;
import fr.ironcraft.mods.phonecraft.blocks.BlockQrCode;
import fr.ironcraft.mods.phonecraft.client.ClientPacketHandler;
import fr.ironcraft.mods.phonecraft.client.PhoneTab;
import fr.ironcraft.mods.phonecraft.common.CommonProxy;
import fr.ironcraft.mods.phonecraft.common.ServerPacketHandler;
import fr.ironcraft.mods.phonecraft.tileentities.TileEntityQrCode;

@Mod(modid = "phonecraft", name = "PhoneCraft", version = "1.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = true,
clientPacketHandlerSpec = @SidedPacketHandler(channels = {"QrCode" }, packetHandler = ClientPacketHandler.class),
serverPacketHandlerSpec = @SidedPacketHandler(channels = {"QrCode" }, packetHandler = ServerPacketHandler.class))

/**
 * @author Dermenslof
 */

public class PhoneCraft
{
	@Instance("phonecraft")
	public static PhoneCraft modInstance;
	
	public static String urlFiles;
	public static String phoneFolder = "./PhoneCraft/";

	public static final CreativeTabs phoneTab = new PhoneTab(CreativeTabs.getNextID(), "PhoneTab");
	public static Block qrCode;
    public static Block flash;

    public static Achievement phoneAchievement = new Achievement(1000, "PhoneCraft", 0, -1, Block.bedrock, null).registerAchievement().setIndependent();

	@SidedProxy(clientSide="fr.ironcraft.mods.phonecraft.client.ClientProxy", serverSide="fr.ironcraft.mods.phonecraft.common.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler 
	public void initConfig(FMLPreInitializationEvent event)
	{
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		urlFiles = config.get(Configuration.CATEGORY_GENERAL, "urlFiles", "http://losan.fr/test/").getString();
		int qrCodeID = config.get(Configuration.CATEGORY_BLOCK, "qrCode", 3002).getInt();
		int flashID = config.get(Configuration.CATEGORY_BLOCK, "flash", 3003).getInt();
		config.save();
		qrCode = new BlockQrCode(qrCodeID).setUnlocalizedName("qrCode").setTextureName("qrCode").setCreativeTab(phoneTab).setStepSound(Block.soundClothFootstep).setBlockUnbreakable();
		flash = new BlockFlash(flashID, Material.glass).setHardness(0.3F).setLightValue(1.0F).setUnlocalizedName("flash").setTextureName("flash");
		proxy.registerRenderThings();
	}

	@EventHandler
	public void load(FMLInitializationEvent event)
	{ 
		GameRegistry.registerBlock(qrCode, "qrCode");
		GameRegistry.registerTileEntity(TileEntityQrCode.class, "QrCode");
	}

	@EventHandler
	public void afterLoad(FMLPostInitializationEvent event)
	{
		System.out.println("[PhoneCraft] Version: 0.1 is loaded");
		
	}

}