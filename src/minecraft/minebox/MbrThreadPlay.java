package minebox;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import cpw.mods.fml.client.FMLClientHandler;

/**
 * @author Dermenslof
 */
public class MbrThreadPlay implements Runnable
{
	Minecraft mc;
	
	public MbrThreadPlay()
	{
		this.mc = FMLClientHandler.instance().getClient();
	}

	@Override
	public void run()
	{
		while (StreamSoundThread.isStart())
		{
			AppMinebox.sound.updateSoundVolume();
			if (mc.currentScreen instanceof GuiMainMenu)
				AppMinebox.sound.interrupt();
		}
	}
}
