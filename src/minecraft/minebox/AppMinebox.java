package minebox;

import fr.ironcraft.mods.phonecraft.api.Application;
import fr.ironcraft.mods.phonecraft.gui.GuiPhoneIngame;

/**
 * @author Thog92
 */
public class AppMinebox implements Application
{
	public static StreamSoundThread sound;
	
	@Override
	public String appname()
	{
		return "Minebox";
	}

	@Override
	public String version()
	{
		return "1.0";
	}

	@Override
	public GuiPhoneIngame ScreenInstance()
	{
		return new GuiPhoneMinebox();
	}
}
